import { AbstractControl, ValidationErrors } from "@angular/forms";

export const validatePasswords = (control: AbstractControl): ValidationErrors | null => {
  if (!control.get('password')?.touched || !control.get('repeatPassword')?.touched) {
    return null;
  }
  const password1: string = control.get('password')?.value;
  const password2: string = control.get('repeatPassword')?.value;
  const error: ValidationErrors | null = password1 === password2 ? null : { PasswordsNotSame: true };
  control.get('repeatPassword')?.setErrors(error);
  return error;
}