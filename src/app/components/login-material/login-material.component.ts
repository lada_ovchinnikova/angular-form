import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, FormGroupName, Validators } from '@angular/forms';
import { validatePasswords } from 'src/app/validators/passwords-validator';



@Component({
  selector: 'app-login-material',
  templateUrl: './login-material.component.html',
  styleUrls: ['./login-material.component.scss']
})
export class LoginMaterialComponent implements OnInit {
  form: FormGroup = new FormGroup({
    account: new FormGroup({
      email: new FormControl('example@mail.ru', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('123Malta', [
        Validators.required,
        Validators.minLength(6)
      ]),
      repeatPassword: new FormControl('123Malta', [
        Validators.required
      ]),
      },
      [validatePasswords]
    ),
    profile: new FormGroup ({
      name: new FormControl('Ivan'),
      phone: new FormControl('8 (000) 000 0000'),
      city: new FormControl('Brashov')
    }),
    company: new FormGroup ({
      companyName: new FormControl('OOO Mashmelow',Validators.required),
      propertyType: new FormControl('corporation',Validators.required),
      inn: new FormControl('123456789', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
      ]),
      kpp:  new FormControl('987654321', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
      ]),
      okpo: new FormControl('56723409', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(8),
      ]),
      date: new FormControl(new Date(2018,0,25))
    }),
    contacts: new FormArray ([]),
  });

  getContactsControls() {
    return (this.form.get('contacts') as FormArray).controls
  }

  addContact(): void {
    (this.form.get('contacts') as FormArray).push(
      new FormGroup ({
        firstname:  new FormControl('Artem', [
          Validators.required, 
        ]),
        position:  new FormControl('manager', [
          Validators.required
        ]),
        phone:  new FormControl('89111111111', [
          Validators.required
        ])
      })
    );
  }

  toggleKppValidators(): void {
    this.form.get('company.propertyType')?.valueChanges.subscribe(
      (propertyType: string) => {
        if (propertyType === 'individual') {
          this.form.get('company.kpp')?.reset();
          this.form.get('company.kpp')?.clearValidators();
          this.form.get('company.kpp')?.updateValueAndValidity();
          return
        }
        this.form.get('company.kpp')?.setValidators([
          Validators.required,
          Validators.minLength(9),
          Validators.maxLength(9)
        ]);
        this.form.get('company.kpp')?.updateValueAndValidity();
      }
    );
  }

  removeContact(indexItem: any): void {
    (this.form.get('contacts') as FormArray).removeAt(indexItem);
  }

  sendForm(form: object): void {
    console.log(this.form.value)
  }

  constructor() { }

  ngOnInit(): void {
    this.toggleKppValidators();

  }
}
